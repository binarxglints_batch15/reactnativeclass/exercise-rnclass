import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  FlatList,
  Alert,
} from 'react-native';
import {useState, useEffect} from 'react';
import Card from '../Bottom/Backdoor';
import Sco from 'react-native-vector-icons/FontAwesome5';
import Banner from '../Header/Banner';
import Bawah from '../Bottom/BoTab';

export default function TodoApp() {
  const [tilte, setTitle] = useState([
    {
      todo: 'Walking',
      description: 'walk',
      status: 'open',
      avatar:
        'https://cdn.icon-icons.com/icons2/2643/PNG/512/male_boy_person_people_avatar_icon_159358.png',
    },
    {
      todo: 'Running',
      description: 'run',
      status: 'done',
      avatar:
        'https://cdn.iconscout.com/icon/free/png-256/avatar-373-456325.png',
    },
    {
      todo: 'Sitting',
      description: 'sit',
      status: 'open',
      avatar:
        'https://cdn.iconscout.com/icon/free/png-256/boy-avatar-4-1129037.png',
    },
  ]);
  const [cari, setCari] = useState('');
  const [todo, setTodo] = useState('');
  const [description, setDescription] = useState('');
  const [status, setStatus] = useState('');
  const [avatar, setAvatar] = useState('');
  const [movies, setMovies] = useState([]);

  useEffect(() => {
    fetch(
      'https://api.themoviedb.org/3/movie/top_rated?api_key=570c36d75740509c00d865a804d826a5&language=en-US&page=1',
    )
      .then(e => {
        // console.log('e 1 ', e);
        return e.json();
      })
      .then(e => {
        // console.log('e 2 ', e);
        setMovies(e.results);
      });
  }, []);
  console.log('movies ', movies);
  return (
    <View style={{backgroundColor: '#ccc', flexWrap: 'wrap'}}>
      <Banner title="Movies" />
      <FlatList
        data={movies}
        style={{margin: 5}}
        numColumns={2}
        renderItem={data => <Card data={data.item} />}
        keyExtractor={(item, i) => i}
      />
      <Bawah />
    </View>
  );
}
