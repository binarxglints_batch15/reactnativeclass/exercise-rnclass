import React, {useState} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import {FloatingLabelInput} from 'react-native-floating-label-input';
import Ent from 'react-native-vector-icons/Entypo';

export default function Signin(props) {
  const [Email, setEmail] = useState('');
  const [cont, setCont] = useState('');
  const [show, setShow] = useState(false);
  return (
    <ImageBackground style={Sss.bg} source={require('../Asset/signup.jpg')}>
      <Text style={Sss.head}>Sign In</Text>
      <View style={Sss.fill}>
        <View style={{marginBottom: 10}}>
          <FloatingLabelInput
            label={'Email'}
            value={Email}
            hintTextColor={'white'}
            staticLabel
            mask="@mail.com"
            hint="Example@mail.com"
            onChangeText={value => setEmail(value)}
            containerStyles={{
              borderWidth: 2,
              paddingHorizontal: 10,
              // backgroundColor: 'grey',
              borderColor: 'white',
              borderRadius: 8,
            }}
            customLabelStyles={{
              colorFocused: 'red',
              fontSizeFocused: 10,
            }}
            labelStyles={{
              paddingHorizontal: 5,
              borderRadius: 5,
            }}
            inputStyles={{
              color: 'white',
              paddingHorizontal: 10,
            }}
            labelStyles={{
              backgroundColor: 'white',
              paddingHorizontal: 5,
              borderRadius: 5,
            }}
          />
        </View>
        <View>
          <FloatingLabelInput
            label={'Password'}
            isPassword
            togglePassword={show}
            staticLabel
            hintTextColor={'white'}
            hint="Password"
            value={cont}
            onChangeText={value => setCont(value)}
            customShowPasswordComponent={
              <Ent name="eye" size={20} color={'white'} />
            }
            customHidePasswordComponent={
              <Ent name="eye-with-line" size={20} color={'white'} />
            }
            containerStyles={{
              borderWidth: 2,
              paddingHorizontal: 10,
              // backgroundColor: 'grey',
              borderColor: 'white',
              borderRadius: 8,
            }}
            customLabelStyles={{
              colorFocused: 'red',
              fontSizeFocused: 10,
            }}
            labelStyles={{
              backgroundColor: 'white',
              paddingHorizontal: 5,
              borderRadius: 5,
            }}
            inputStyles={{
              color: 'white',
              paddingHorizontal: 5,
            }}
          />
        </View>
        <TouchableOpacity
          style={Sss.btn}
          onPress={() => {
            props.navigation.navigate('Bottom');
          }}>
          <Text style={Sss.txtbtn}>Sign In</Text>
        </TouchableOpacity>
        <Text style={{textAlign: 'center'}}>
          Don't have an Account ?{' '}
          <Text
            style={Sss.signin}
            onPress={() => {
              props.navigation.navigate('Sign Up');
            }}>
            Sign Up
          </Text>
        </Text>
      </View>
    </ImageBackground>
  );
}

const Sss = StyleSheet.create({
  bg: {
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 50,
    height: '100%',
  },
  head: {
    fontSize: 30,
    fontFamily: 'LBT900',
    marginBottom: 40,
    color: 'white',
  },
  Input: {
    fontSize: 12,
    fontFamily: 'LBT900',
    width: '100%',
  },
  fill: {
    width: '100%',
    marginBottom: 40,
  },
  section: {
    flexDirection: 'row',
    paddingVertical: 1,
    borderWidth: 1,
    borderColor: 'white',
    height: 40,
    margin: 5,
    borderRadius: 10,
  },
  icostyle: {
    padding: 5,
  },
  txtbtn: {
    fontFamily: 'LBT900',
    color: 'white',
  },
  btn: {
    width: '100%',
    borderRadius: 100,
    borderWidth: 2,
    borderColor: 'white',
    shadowOpacity: 1,
    shadowColor: 'black',
    shadowOffset: {
      height: 1,
      width: 1,
    },
    shadowRadius: 2,
    paddingVertical: 10,
    marginBottom: 20,
    alignItems: 'center',
    marginTop: 30,
  },
  signin: {
    textDecorationLine: 'underline',
    color: 'blue',
  },
});
