import React, {useState} from 'react';
import {View, Text, StyleSheet} from 'react-native';

let Header = props => {
  //   const [nama, setNama] = useState('Elon');

  // console.log(props);

  return (
    <View style={styles.container}>
      <Text style={styles.txtHeader}>{props.title}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: 'black',
    padding: 10,
    alignItems: 'center',
    elevation: 7,
  },
  txtHeader: {
    color: 'red',
    fontSize: 25,
    fontFamily: 'LBT900',
  },
});

export default Header;
