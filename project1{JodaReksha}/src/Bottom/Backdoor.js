import React from 'react';
import {View, Text, Image, StyleSheet} from 'react-native';

export default function Backdoor({data}) {
  console.log('data ', data);
  return (
    <View>
      <View style={style.Contain}>
        <View style={style.cardbg}>
          <Image
            source={{uri: `https://image.tmdb.org/t/p/w500${data.poster_path}`}}
            style={style.img}
          />
          <Text style={style.title}>{data.title}</Text>
          <Text>Rate:{data.popularity}</Text>
          <Text>Release: {data.release_date}</Text>
          <Text>
            Sumary<Text style={{color: 'white'}}> .................... </Text>
          </Text>
        </View>
      </View>
    </View>
  );
}
const style = StyleSheet.create({
  Contain: {
    padding: 13,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
  },
  img: {
    borderRadius: 8,
    height: 200,
    width: 140,
    marginBottom: 10,
  },
  cardbg: {
    elevation: 7,
    paddingHorizontal: 5,
    paddingVertical: 10,
    borderRadius: 8,
    alignItems: 'center',
    backgroundColor: '#fcfcfc',
    marginBottom: 5,
  },
  title: {
    fontSize: 10,
    color: 'black',
    fontWeight: 'bold',
    marginBottom: 7,
    maxWidth: '45%',
    textAlign: 'center',
  },
});
