import React, {useState, useEffect} from 'react';
import {Image, View} from 'react-native';
import {FloatingLabelInput} from 'react-native-floating-label-input';

export default function App() {
  const [phone, setPhone] = useState('');
  const [Name, setName] = useState('');
  const [Email, setEmail] = useState('');

  return (
    <View
      style={{
        padding: 50,
        flex: 1,
        backgroundColor: 'black',
        justifyContent: 'center',
      }}>
      <Image
        style={{
          height: 200,
          width: 200,
          borderRadius: 100,
          alignSelf: 'center',
          marginVertical: 20,
        }}
        source={require('../Asset/profile.jpeg')}
      />
      <FloatingLabelInput
        label={'Full Name'}
        value={Name}
        staticLabel
        hintTextColor={'#aaa'}
        hint="Full Name"
        onChangeText={value => setName(value)}
        containerStyles={{
          borderWidth: 2,
          paddingHorizontal: 10,
          backgroundColor: 'grey',
          borderColor: 'white',
          borderRadius: 8,
        }}
        customLabelStyles={{
          colorFocused: 'red',
          fontSizeFocused: 10,
        }}
        labelStyles={{
          backgroundColor: 'white',
          paddingHorizontal: 5,
          borderRadius: 5,
        }}
        inputStyles={{
          color: 'white',
          paddingHorizontal: 5,
        }}
      />

      <View style={{marginTop: 10}}>
        <FloatingLabelInput
          label="Phone"
          value={phone}
          staticLabel
          hintTextColor={'#aaa'}
          mask="+62 (81) 99999-9999"
          hint="+62 (81) 98765-4321"
          containerStyles={{
            borderWidth: 2,
            paddingHorizontal: 10,
            backgroundColor: 'grey',
            borderColor: 'white',
            borderRadius: 8,
          }}
          customLabelStyles={{
            colorFocused: 'red',
            fontSizeFocused: 12,
          }}
          labelStyles={{
            backgroundColor: 'white',
            paddingHorizontal: 5,
            borderRadius: 5,
          }}
          inputStyles={{
            color: 'white',
            paddingHorizontal: 10,
          }}
          onChangeText={value => {
            setPhone(value);
          }}
        />
        <View style={{marginTop: 10}}>
          <FloatingLabelInput
            label={'Email'}
            value={Email}
            hintTextColor={'#aaa'}
            staticLabel
            mask="@mail.com"
            hint="Example@mail.com"
            onChangeText={value => setEmail(value)}
            containerStyles={{
              borderWidth: 2,
              paddingHorizontal: 10,
              backgroundColor: 'grey',
              borderColor: 'white',
              borderRadius: 8,
            }}
            customLabelStyles={{
              colorFocused: 'red',
              fontSizeFocused: 10,
            }}
            labelStyles={{
              paddingHorizontal: 5,
              borderRadius: 5,
            }}
            inputStyles={{
              color: 'white',
              paddingHorizontal: 10,
            }}
            labelStyles={{
              backgroundColor: 'white',
              paddingHorizontal: 5,
              borderRadius: 5,
            }}
          />
        </View>
      </View>
    </View>
  );
}
