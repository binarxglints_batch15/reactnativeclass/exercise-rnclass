import React from 'react';
import {View, Text} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import Ent from 'react-native-vector-icons/Entypo';
import ACT from 'react-native-vector-icons/Feather';
import Evil from 'react-native-vector-icons/EvilIcons';
export default function BoTab() {
  return (
    <View>
      <View
        style={{
          backgroundColor: 'white',
          height: 50,
          margin: 0,
          elevation: 7,
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-around',
            padding: 5,
            margin: 5,
            alignItems: 'center',
          }}>
          <View>
            <Icon name="home" size={22} color="black" />
            <View>
              <Text style={{fontSize: 9, fontFamily: 'LB900', color: 'black'}}>
                Home
              </Text>
            </View>
          </View>
          <View>
            <ACT name="activity" size={22} color="black" />
            <View>
              <Text style={{fontSize: 9, fontFamily: 'LB900', color: 'black'}}>
                Activity
              </Text>
            </View>
          </View>
          <View>
            <Ent name="chat" size={22} color="black" />
            <View>
              <Text style={{fontSize: 9, fontFamily: 'LB900', color: 'black'}}>
                Chat
              </Text>
            </View>
          </View>
          <View>
            <Evil name="user" size={30} color="black" />
            <View>
              <Text style={{fontSize: 9, fontFamily: 'LB900', color: 'black'}}>
                Profile
              </Text>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
}
