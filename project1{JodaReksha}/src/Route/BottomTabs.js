import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  ImageBackground,
} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import GopayD from '../Content/GopayD';
import App from '../Content/App1';
import Log from './Logout';
import Profile from '../Bottom/Profile';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Fea from 'react-native-vector-icons/Feather';
import {LogBox} from 'react-native';
LogBox.ignoreLogs(['Reanimated 2']);
LogBox.ignoreLogs(['Consol Warning']);
LogBox.ignoreLogs(['Consol Warning']);
function SettingsScreen({navigation}) {
  return (
    <ImageBackground style={style.bg} source={require('../Asset/signup.jpg')}>
      <View
        style={{
          flex: 1,
          justifyContent: 'space-evenly',
          alignItems: 'center',
          flexDirection: 'row',
        }}>
        <TouchableOpacity
          onPress={() => navigation.navigate('Profile')}
          style={style.btn}>
          <Text style={style.btntxt}>Profile </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => navigation.navigate('Logout')}
          style={style.btn}>
          <Text style={style.btntxt}>Sign Out</Text>
        </TouchableOpacity>
      </View>
    </ImageBackground>
  );
}
const style = StyleSheet.create({
  bg: {
    height: '100%',
    flex: 1,
  },
  btntxt: {
    textAlign: 'center',
    padding: 10,
    color: 'black',
    fontFamily: 'LB900',
    textShadowRadius: 10,
  },
  btn: {
    height: 40,
    width: 100,
    borderRadius: 15,
    boxShadow: '10px 10px 17px -12px rgba(0,0,0,0.75)',
    shadowRadius: 30,
    shadowColor: 'white',
    shadowOpacity: 10,
    borderWidth: 1,
    borderColor: 'white',
  },
});
const Stack = createStackNavigator();
function SettingsStack() {
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      <Stack.Screen name="settings" component={SettingsScreen} />
      <Stack.Screen name="Logout" component={Log} />
      <Stack.Screen
        options={{headerShown: true}}
        name="Profile"
        component={Profile}
      />
    </Stack.Navigator>
  );
}

const BottomTabs = createMaterialBottomTabNavigator();
export default function BottomTab({navigation}) {
  return (
    <BottomTabs.Navigator
      screenOptions={{headerShown: false}}
      initialRouteName="HomePay"
      activeColor="white"
      inactiveColor="black"
      barStyle={{backgroundColor: 'skyblue'}}>
      <BottomTabs.Screen
        name="HomePay"
        component={GopayD}
        initialParams={true}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({color, size}) => <Icon name="home" size={27} />,
        }}
      />

      <BottomTabs.Screen
        name="TO"
        component={App}
        options={{
          tabBarLabel: 'TopUp',
          tabBarIcon: ({color, size}) => <Fea name="credit-card" size={24} />,
        }}
      />
      <BottomTabs.Screen
        name="Settings"
        component={SettingsStack}
        options={{
          tabBarLabel: 'Settings',
          tabBarIcon: ({color, size}) => <Fea name="settings" size={24} />,
        }}
      />
    </BottomTabs.Navigator>
  );
}
