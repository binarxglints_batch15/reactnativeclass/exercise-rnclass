import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  ImageBackground,
} from 'react-native';

export default function Logout({navigation}) {
  return (
    <ImageBackground source={require('../Asset/14271.jpg')} style={style.bg}>
      <View>
        <Text style={{fontFamily: 'LBT900'}}>
          Are You Sure Want to SignOut?
        </Text>
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('Hello');
          }}
          style={style.btn}>
          <Text style={style.btntxt}>Logout</Text>
        </TouchableOpacity>
      </View>
    </ImageBackground>
  );
}
const style = StyleSheet.create({
  bg: {
    flex: 1,
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
  },
  btntxt: {
    textAlign: 'center',
    padding: 10,
    color: 'white',
    fontFamily: 'LB900',
  },
  btn: {
    backgroundColor: 'red',
    height: 40,
    width: 100,
    borderRadius: 15,
    margin: 20,
    alignSelf: 'center',
  },
});
