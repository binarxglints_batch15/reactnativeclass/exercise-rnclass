import {createStackNavigator} from '@react-navigation/stack';
import React from 'react';
import Hello from '../Header/Hello';
import SignUp from '../Header/SignUp';
import SignIn from '../Header/Signin';
import Draw from './Drawer';
import Bot from './BottomTabs';
import log from './Logout';

function AppsStack() {
  const Stack = createStackNavigator();
  return (
    <Stack.Navigator screenOptions={{headerShown: false}}>
      {/* <Stack.Screen name="Fro" component={Draw} /> */}

      <Stack.Screen
        options={{headerShown: false}}
        name="Hello"
        component={Hello}
      />
      <Stack.Screen name="Sign Up" component={SignUp} />
      <Stack.Screen name="Sign In" component={SignIn} />
      <Stack.Screen name="Bottom" component={Bot} />
    </Stack.Navigator>
  );
}
export default AppsStack;
