/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import Movie from './src/Content/Movie';
import Home from './Home';
AppRegistry.registerComponent(appName, () => App);
