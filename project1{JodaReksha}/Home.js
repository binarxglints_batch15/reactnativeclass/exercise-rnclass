import React, {useEffect, useState} from 'react';
import {View, Text, StyleSheet, Image, ScrollView} from 'react-native';
// import Header from '../components/Header';
import axios from 'axios';

let Home = props => {
  const [data, setData] = useState([]);

  useEffect(() => {
    // GET => Mengambil data
    // fetch('http://www.omdbapi.com/?apikey=9a7699d9&s=avengers')
    //     .then(response => response.json())
    //     .then(res => setData(res.Search))
    //     .catch(error => console.log(error))

    axios
      .get('http://www.omdbapi.com/?apikey=9a7699d9&s=justice league')
      .then(res => {
        console.log('res1', res);
        setData(res.data.Search);
      })
      .catch(err => console.log(err));

    // POST => membuat data baru
    // axios.post('http://10.0.2.2:3004/users', {
    //     nama: 'Nanang',
    //     umur: 10,
    //     alamat: 'Sumbawa'
    // })
    //     .then(res => console.log(res.data))
    //     .catch(err => console.log(err))

    // PUT/PATCH => merubah data
    // axios.put('http://10.0.2.2:3004/users/4', {
    //     nama: 'Bagas',
    //     umur: 20,
    //     alamat: 'Bandung'
    // })
    //     .then(res => console.log(res.data))
    //     .catch(err => console.log(err))

    // axios.patch('http://10.0.2.2:3004/users/4', {
    //     nama: 'Hasan'
    // })
    //     .then(res => console.log(res.data))
    //     .catch(err => console.log(err))

    // DELETE => menghapus data
    // axios.delete('http://10.0.2.2:3004/users/4')
    //     .then(res => console.log(res.data))
    //     .catch(err => console.log(err))
  }, []);

  return (
    <View style={{flex: 1}}>
      {/* <Header title="Home" /> */}
      <ScrollView style={{backgroundColor: '#ccc'}}>
        <View style={styles.container}>
          {data.map((item, index) => (
            <View key={index} style={styles.card}>
              <Image source={{uri: item.Poster}} style={styles.img} />
              <Text style={styles.title}>{item.Title}</Text>
              <Text>{item.Year}</Text>
              <Text>{item.Type}</Text>
            </View>
          ))}
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 13,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
  },
  card: {
    elevation: 7,
    paddingHorizontal: 10,
    paddingVertical: 10,
    borderRadius: 8,
    alignItems: 'center',
    width: '49%',
    backgroundColor: '#fcfcfc',
    marginBottom: 15,
  },
  img: {
    borderRadius: 8,
    height: 200,
    width: 150,
    marginBottom: 10,
  },
  title: {
    fontSize: 18,
    color: 'black',
    fontWeight: 'bold',
    marginBottom: 7,
  },
});

export default Home;
