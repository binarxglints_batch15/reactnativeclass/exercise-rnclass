import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {baseProps} from 'react-native-gesture-handler/lib/typescript/handlers/gestureHandlers';
const Profile = props => {
  return (
    <View style={styles.container}>
      <Text style={styles.profile}>Profile</Text>
      <View style={styles.picture}>
        <Text style={{alignSelf: 'center', fontSize: 50}}>INI FOTO</Text>
      </View>
      <View>
        <View style={styles.backtxt}>
          <Text style={styles.text}>Full Name</Text>
          <Text style={styles.input}>Elon Musk</Text>
        </View>
        <View style={styles.backtxt}>
          <Text style={styles.text}>Phone</Text>
          <Text style={styles.input}>+62 812 3456 7890</Text>
        </View>
        <View style={styles.backtxt}>
          <Text style={styles.text}>Email</Text>
          <Text style={styles.input}>elon.musk@gmail.com</Text>
        </View>
      </View>
      <TouchableOpacity
        onPress={() => props.navigation.navigate('Register')}
        style={styles.logOutBtn}>
        <Text style={styles.logOut}>Log Out</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#0f1a26',
    flex: 1,
    padding: 15,
  },
  profile: {
    fontWeight: 'bold',
    fontSize: 37,
    color: 'white',
  },
  picture: {
    height: 250,
    width: 250,
    backgroundColor: 'blue',
    borderRadius: 150,
    alignSelf: 'center',
    justifyContent: 'center',
    marginTop: 15,
  },
  backtxt: {
    backgroundColor: 'grey',
    marginTop: 25,
    borderRadius: 10,
    paddingVertical: 5,
    paddingHorizontal: 15,
  },
  text: {
    fontSize: 14,
    color: 'white',
  },
  input: {
    color: 'white',
    fontSize: 24,
  },
  logOutBtn: {
    backgroundColor: '#00E1EF',
    borderRadius: 25,
    width: 200,
    height: 57,
    alignSelf: 'center',
    justifyContent: 'center',
    marginTop: 30,
  },
  logOut: {
    fontSize: 30,
    alignSelf: 'center',
    fontWeight: 'bold',
    color: 'black',
  },
});

export default Profile;
