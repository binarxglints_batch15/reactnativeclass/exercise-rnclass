import React from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  TextInput,
} from 'react-native';
import {baseProps} from 'react-native-gesture-handler/lib/typescript/handlers/gestureHandlers';
import {FloatingLabelInput} from 'react-native-floating-label-input';

const SignIn = props => {
  return (
    <View style={styles.container}>
      <View style={styles.contents}>
        <Text style={styles.homeLabel}>Please Sign In</Text>
        <View>
          <TextInput placeholder="Your Mail" style={styles.input} />
          <TextInput placeholder="Password" style={styles.input} />
          <TextInput placeholder="Confirm Password" style={styles.input} />
        </View>
        <View style={styles.btnwrp}>
          <TouchableOpacity
            style={styles.btn}
            onPress={() => props.navigation.navigate('MainTab')}>
            <Text style={styles.textbtn}>Sign In</Text>
          </TouchableOpacity>
        </View>
        <View
          style={{flexDirection: 'row', alignSelf: 'center', marginTop: 20}}>
          <Text style={styles.regist}>Didn't have Account? Please Create </Text>
          <TouchableOpacity
            onPress={() => props.navigation.navigate('Register')}>
            <Text style={styles.sign}>here.</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#0f1a26',
    flex: 1,
    justifyContent: 'center',
  },
  contents: {
    marginHorizontal: 20,
    alignContent: 'center',
    borderRadius: 40,
    paddingVertical: 15,
  },
  homeLabel: {
    fontSize: 40,
    fontWeight: '700',
    color: 'white',
    marginVertical: 2,
  },
  btnwrp: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    textAlignVertical: 'center',
  },
  btn: {
    height: 57,
    width: 200,
    marginTop: 50,
  },
  textbtn: {
    fontSize: 27,
    textAlign: 'center',
    fontWeight: 'bold',
    color: 'black',
    flex: 1,
    textAlignVertical: 'center',
    backgroundColor: '#00E1EF',
    borderRadius: 25,
  },
  input: {
    height: 57,
    marginTop: 30,
    borderRadius: 13,
    backgroundColor: '#626262',
  },
  regist: {
    textAlign: 'center',
    fontSize: 17,
    color: 'white',
  },
  sign: {
    color: '#00E1EF',
    fontSize: 17,
    fontWeight: 'bold',
  },
});

export default SignIn;
