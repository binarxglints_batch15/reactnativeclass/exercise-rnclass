import React from 'react';
import {View, Text, StyleSheet, ScrollView} from 'react-native';

const Home = navigation => {
  return (
    <ScrollView>
      <View style={styles.container}>
        <Text style={styles.home}>Home</Text>
        <Text style={styles.label}>Up Coming Movie</Text>
        <View style={styles.header}></View>
        <Text style={styles.label}>Popular movie</Text>
        <View style={styles.main}>
          <View style={styles.item}></View>
          <View style={styles.item}></View>
        </View>
        <Text style={styles.label}>Last Watching</Text>
        <View style={styles.header}></View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#0f1a26',
    padding: 20,
  },
  home: {
    fontWeight: 'bold',
    fontSize: 37,
    color: 'white',
  },
  label: {
    color: 'white',
    fontSize: 17,
    fontWeight: 'bold',
    paddingBottom: 8,
  },

  header: {
    backgroundColor: '#C4C4C4',
    height: 180,
    marginBottom: 20,
  },
  main: {
    height: 230,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 20,
  },
  item: {
    width: '45%',
    backgroundColor: '#C4C4C4',
    alignItems: 'center',
  },
});

export default Home;
