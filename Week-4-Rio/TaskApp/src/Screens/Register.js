import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  TextInput,
  StyleSheet,
  Button,
  TouchableOpacity,
} from 'react-native';

const Register = ({navigation}) => {
  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <Text style={styles.label}>Create Account</Text>
        <Text style={{color: 'white', fontSize: 18}}>
          Please fill in the input below
        </Text>
      </View>

      <View style={styles.content}>
        <TextInput style={styles.input} placeholder="Full Name" />
        <TextInput style={styles.input} placeholder="Phone" />
        <TextInput style={styles.input} placeholder="Email" />
        <TextInput style={styles.input} placeholder="Password" />
        <TextInput style={styles.input} placeholder="Confirm Password" />
      </View>

      <TouchableOpacity
        onPress={() => navigation.navigate('MainTab')}
        style={styles.signBtn}>
        <Text style={styles.signUp}>Sign Up</Text>
      </TouchableOpacity>

      <View style={{flexDirection: 'row', alignSelf: 'center'}}>
        <Text style={styles.regist}>Account was Ready Create, </Text>
        <TouchableOpacity onPress={() => navigation.navigate('SignIn')}>
          <Text style={styles.signIn}>Sign In</Text>
        </TouchableOpacity>
        <Text style={styles.regist}> here.</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    alignContent: 'center',
    flex: 1,
    paddingHorizontal: 15,
    backgroundColor: '#0f1a26',
  },

  header: {
    marginTop: 50,
  },

  label: {
    fontSize: 40,
    fontWeight: '700',
    color: 'white',
  },
  content: {
    marginTop: 30,
  },
  input: {
    fontSize: 20,
    marginBottom: 30,
    backgroundColor: '#626262',
    borderRadius: 10,
    height: 57,
  },
  signBtn: {
    backgroundColor: '#00E1EF',
    borderRadius: 25,
    width: 200,
    height: 57,
    alignSelf: 'center',
    justifyContent: 'center',
    marginBottom: 20,
  },
  signUp: {
    fontSize: 30,
    alignSelf: 'center',
    fontWeight: 'bold',
    color: 'black',
  },
  regist: {
    textAlign: 'center',
    fontSize: 17,
    color: 'white',
  },
  signIn: {
    color: '#00E1EF',
    fontSize: 17,
    fontWeight: 'bold',
    borderBottomColor: '#00E1EF',
    borderBottomWidth: 1,
  },
});

export default Register;
