import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Home from '../Screens/Home';
import Profile from '../Screens/Profile';
import Hom from 'react-native-vector-icons/MaterialCommunityIcons';
import Acc from 'react-native-vector-icons/FontAwesome';

const HomeStack = createBottomTabNavigator();
const ProfileStack = createBottomTabNavigator();
const Tab = createBottomTabNavigator();

const HomeStackScreen = () => {
  <HomeStack.Navigator>
    <HomeStack.Screen name="Home" component={Home} />
  </HomeStack.Navigator>;
};

const ProfileStackScreen = () => {
  <ProfileStack.Navigator>
    <ProfileStack.Screen name="Profile" component={Profile} />
  </ProfileStack.Navigator>;
};

const MainTab = props => {
  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({color, size}) => (
            <Hom name="home" size={35} color="black" />
          ),
          tabBarStyle: {backgroundColor: '#00E1EF'},
        }}
      />
      <Tab.Screen
        name="Profile"
        component={Profile}
        options={{
          tabBarLabel: 'Profile',
          tabBarIcon: ({color, size}) => (
            <Acc name="user" size={30} color="black" />
          ),
          tabBarStyle: {backgroundColor: '#00E1EF'},
        }}
      />
    </Tab.Navigator>
  );
};

export default MainTab;
